import { styled } from '@smooth-ui/core-em';
import React from 'react';
import PropTypes from 'prop-types';
import { theme } from '../../theme/default';

const StyledPageWrapper = styled.div`
  min-height: calc( 100vh - ${theme.headerHeight});
  padding: 1rem;
  background-color: ${theme.primaryLighter};
  margin-top: ${theme.headerHeight};
  .page-inner {
    max-width: 500px;
    display: block;
    margin: 0 auto;
    @media (min-width: 981px) {
      max-width: 750px;
    }
  }
`;


const PageWrapper = ({ children }) => (
  <StyledPageWrapper>
    <div className="page-inner">
      {children}
    </div>
  </StyledPageWrapper>
);

PageWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};


export default PageWrapper;
