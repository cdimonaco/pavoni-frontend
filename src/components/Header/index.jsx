/* eslint-disable jsx-a11y/click-events-have-key-events */
/*  eslint-disable jsx-a11y/no-noninteractive-element-interactions */

import React from 'react';
import { styled } from '@smooth-ui/core-em';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBus, faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { theme } from '../../theme/default';

const StyledHeader = styled.div`
    background-color: ${theme.primary};
    display: flex;
    align-items: center;
    justify-content: flex-start;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 100;
    
    .app-name {
        height: ${theme.headerHeight};
        color: ${theme.almostWhite};
        display: flex;
        align-items: center;
        justify-content: center;
        align-items: center;
        flex: 1;
        font-size: 26px;
        font-weight: bold;
        svg {
          margin-left: 5px;
        }
    }

`;
const MenuToggler = styled.div`
  width: 70px;
  background-color: ${theme.secondary};
  height: 70px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 30px;
  color: white;
  cursor: pointer;

  .burger {
    display: block;
  }

  .times {
    display: none;
  }

  &.open {
    .burger {
      display: none;
    }
    .times {
      display: block;
    }
  }

`;

const Menu = styled.ul`
  position: absolute;
  top: ${theme.headerHeight};
  width: 100vw;
  transform: translate(-100%, 0);
  background-color: ${theme.secondary};
  transition: transform 0.5s ${theme.easticEase}; 
  border-top: 2px solid ${theme.almostWhite};
  padding: 1rem;
  margin: 0;
  list-style: none;
  z-index: 10;

   @media (min-width: 981px) {
      max-width: 400px;
    }
  &.open {
      transform: translate(0%, 0);
  }

  li {
    display: block;
    padding: 1rem 0.5rem;
    color: ${theme.almostWhite};
  }

  
`;

const Header = (props) => {
  const [menuIsOpen, toggleMenu] = React.useState(false);
  const { history } = props;

  const handleToggle = (path) => {
    toggleMenu(false);
    history.push(path);
  };

  return (
    <StyledHeader>
      <MenuToggler className={menuIsOpen ? 'open' : ''} onClick={() => toggleMenu(!menuIsOpen)}>
        <div className="burger">
          <FontAwesomeIcon icon={faBars} />
        </div>
        <div className="times">
          <FontAwesomeIcon icon={faTimes} />
        </div>
      </MenuToggler>
      <Menu className={menuIsOpen ? 'open' : ''}>
        <li onClick={() => handleToggle('/dashboard')}>DASHBOARD</li>
        <li onClick={() => handleToggle('/location')}>VICINO A ME</li>
        <li onClick={() => handleToggle('/checkin')}>CHECKIN</li>
        <li>STORICO PAGAMENTI</li>
      </Menu>
      <span
        onClick={() => history.push('/')}
        role="menu"
        tabIndex={0}
        className="app-name"
      >
        SmarTicket
        {' '}
        <FontAwesomeIcon icon={faBus} />
      </span>
    </StyledHeader>
  );
};

Header.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};


export default withRouter(Header);
