/* eslint-disable jsx-a11y/click-events-have-key-events */
/*  eslint-disable jsx-a11y/no-noninteractive-element-interactions */

import React from 'react';
import {
  Marker, InfoWindow,
} from 'react-google-maps';
import PropTypes from 'prop-types';
import BusStop from './bus-stop.svg';
import TrainStop from './train-stop.svg';

const BusStopMarker = (props) => {
  const {
    position: {
      name, location: { lat, lon: lng }, company, id,
    },
  } = props;
  const [infoOpen, toggleInfoOpen] = React.useState(false);

  const icon = company.toLowerCase() === 'fer' ? TrainStop : BusStop;
  const size = company.toLowerCase() === 'fer' ? 30 : 45;
  return (
    <Marker
      icon={{
        url: icon,
        size: new window.google.maps.Size(size, size),
        origin: new window.google.maps.Point(0, 0),
        anchor: new window.google.maps.Point(15, 34),
        scaledSize: new window.google.maps.Size(size, size),
      }}
      position={{ lat, lng }}
      onClick={() => toggleInfoOpen(!infoOpen)}
    >
      {infoOpen && (
        <>
          <div
            onClick={() => toggleInfoOpen(false)}
            role="menu"
            tabIndex={0}
            style={{
              position: 'absolute', top: 0, right: 0, left: 0, bottom: 0, background: 'transparent',
            }}
          />
          <InfoWindow>
            <div>
              {name}
              {' '}
              {id}
            </div>
          </InfoWindow>
        </>
      )}
    </Marker>
  );
};

BusStopMarker.propTypes = {
  position: PropTypes.shape({
    name: PropTypes.string.isRequired,
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
    company: PropTypes.string,
  }).isRequired,
};


export default BusStopMarker;
