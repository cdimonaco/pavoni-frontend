import React from 'react';
import {
  withScriptjs, withGoogleMap, GoogleMap, Marker,
} from 'react-google-maps';
import PropTypes from 'prop-types';
import UserPinned from './user-pinned.svg';


const MyMapComponent = (props) => {
  const { userPosition: { latitude, longitude }, positions, MarkerComponent } = props;

  if (!latitude || !longitude) {
    return null;
  }

  return (
    <GoogleMap
      defaultZoom={16}
      defaultCenter={{ lat: latitude, lng: longitude }}
      defaultOptions={{ mapTypeControl: false }}
      {...props}
    >
      {positions.map(position => <MarkerComponent position={position} key={position.id} />)}

      {/* USER MARKER */}
      <Marker
        icon={{
          url: UserPinned,
          size: new window.google.maps.Size(45, 45),
          origin: new window.google.maps.Point(0, 0),
          anchor: new window.google.maps.Point(45, 45),
          scaledSize: new window.google.maps.Size(45, 45),
        }}
        position={{ lat: latitude, lng: longitude }}
      />
    </GoogleMap>
  );
};

MyMapComponent.propTypes = {
  MarkerComponent: PropTypes.func.isRequired,
  userPosition: PropTypes.shape({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
  }).isRequired,
  positions: PropTypes.arrayOf(
    PropTypes.shape({
      location: PropTypes.shape({
        lat: PropTypes.number,
        lon: PropTypes.number,
      }).isRequired,
      id: PropTypes.string.isRequired,
      company: PropTypes.string.isRequired,
    }),
  ).isRequired,
};
const EnhancedMap = withScriptjs(withGoogleMap(MyMapComponent));

export default EnhancedMap;
