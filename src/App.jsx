import React from 'react';
import { Normalize } from '@smooth-ui/core-em';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Login from './screens/Login/index';
import AppRoutes from './routes';

function App() {
  return (
    <div className="App">
      <Normalize />
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/" component={AppRoutes} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
