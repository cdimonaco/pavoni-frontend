import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PageWrapper from './components/PageWrapper';
import Header from './components/Header';
import Dashboard from './screens/Dashboard/index';
import CloseToYou from './screens/CloseToYou/index';
import CheckIn from './screens/CheckIn';
import LinesList from './screens/Lines';
import RealTimeFlow from './screens/RealTimeFlow';

const AppRoutes = () => (
  <>
    <Header />
    <PageWrapper>
      <Switch>
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/location" component={CloseToYou} />
        <Route exact path="/checkin" component={CheckIn} />
        <Route exact path="/stops/:stopId/lines" component={LinesList} />
        <Route exact path="/stops/:stopId/lines/:lineId" component={RealTimeFlow} />
      </Switch>
    </PageWrapper>
  </>
);

export default AppRoutes;
