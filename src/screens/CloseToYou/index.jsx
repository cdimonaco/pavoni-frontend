/* eslint-disable no-useless-escape */
import React from 'react';
import { styled } from '@smooth-ui/core-em';
import { Link } from 'react-router-dom';
import { useGeolocation } from 'react-use';
import googleMapApiKey from '../../constants/googleMapApiKey';
import LoadingSpinner from '../../components/LoadingSpinner';
import EnhancedMap from '../../components/Maps/EnhancedMap';
import BusStopMarker from '../../components/Maps/BusStopMarker';

const LocationWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  gap: 2rem;

  @media (min-width: 768px) {
      grid-template-columns: 2fr 1fr;
      gap: 2rem;
  }
`;

const StopList = styled.div`
  display: flex;
  flex-direction: column;
  margin: 1rem 0 0;
  padding: 0;
  @media (min-width: 768px) {
    margin: 0;
  }
  a {
    padding: 0.5rem 0;
    text-decoration: none;
    color: inherit;
  }
`;


const fetchPositions = (
  latitude,
  longitude,
) => window.fetch(
  `https://apimover.soon.it/api/stops_nearby\?lat\=${latitude}\&lon\=${longitude}`,
  { mode: 'cors' },
).then(r => r.json())
  .catch(() => ({ docs: [], ok: false }));

const parsePositions = docs => docs.map(p => ({
  location: p.location, name: p.name, company: p.company, id: p.id,
}));


const CloseToYou = () => {
  const geolocationState = useGeolocation({ enableHighAccuracy: true });
  const { latitude, longitude } = geolocationState;
  const [stops, setStops] = React.useState(null);

  React.useEffect(() => {
    // When a position is retrieved
    if (latitude && longitude) {
      fetchPositions(latitude, longitude).then(
        ({ docs, ok }) => {
          if (ok) setStops(parsePositions(docs));
        },
      );
    }
  }, [latitude, longitude]);

  console.log(stops);
  return (
    <>
      <h2>Le fermate vicino a te</h2>
      <LocationWrapper>
        {geolocationState.latitude
          ? (
            <div>
              <EnhancedMap
                googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${googleMapApiKey}`}
                loadingElement={<div style={{ height: '100%' }} />}
                containerElement={<div style={{ height: '300px' }} />}
                mapElement={<div style={{ height: '100%' }} />}
                userPosition={{
                  latitude,
                  longitude,
                }}
                positions={stops || []}
                MarkerComponent={BusStopMarker}
              />
            </div>
          )
          : <LoadingSpinner />}

        <StopList>
          {stops && stops.length && stops.map(stop => (
            <Link
              to={{
                pathname: `/stops/${stop.id}/lines`,
                state: {
                  currentStop: {
                    name: stop.name,
                    company: stop.company,
                  },
                },
              }}
              key={stop.id}
            >
              {stop.name}
              {' '}
              {stop.id}
            </Link>
          ))}
        </StopList>
      </LocationWrapper>
    </>
  );
};

export default CloseToYou;
