import React from 'react';
import styled from '@emotion/styled';
import TimeLine from './Timeline';
import { theme } from '../../theme/default';

const CheckInReview = styled.div`
  display: flex;
  flex-direction: column;
`;

const ReviewItem = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0.5rem 1rem;
  span:first-of-type {
    font-weight: bold;
  }
  span {
    flex: 1;
    text-align: left;
  }

  &.active {
    background-color: ${theme.success};
    color: #fff;
  }
`;

const CheckIn = () => (
  <React.Fragment>
    <h2>Il tuo viaggio</h2>
    <CheckInReview>
      <ReviewItem>
        <span>Fermata di partenza</span>
        <span>Carracci Stazione AV</span>
      </ReviewItem>
      <ReviewItem className="active">
        <span>Prossima fermata</span>
        <span>Indipendenza</span>
      </ReviewItem>
      <ReviewItem>
        <span>Linea</span>
        <span>A</span>
      </ReviewItem>
      <ReviewItem>
        <span>Direzione</span>
        <span>Piazza Cavour</span>
      </ReviewItem>
      <ReviewItem>
        <span>Tempo trascorso</span>
        <span>7mins</span>
      </ReviewItem>
    </CheckInReview>
    <TimeLine />
  </React.Fragment>
);

export default CheckIn;
