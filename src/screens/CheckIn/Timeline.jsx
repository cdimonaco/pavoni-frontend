import React from 'react';
import styled from '@emotion/styled';
import { faFlag, faFlagCheckered } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Timeline, TimelineEvent } from 'react-event-timeline';
import { theme } from '../../theme/default';

const TimeLineContainer = styled.div`
  margin-top: 2rem;
  border-top: 2px solid black;

  
`;

const StopInstruction = styled.div`
  display: flex;
  font-size: 14px;

  
  .label {
    width: 50%;
  }
  .value {
    &.medium {
      color: #ff9100;
    }
    &.high {
      color: red;
    }
  }
`;

const StopInstructionContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const TimeLine = () => (
  <TimeLineContainer>
    <Timeline
      lineColor="black"
      lineStyle={{
        'margin-top': '0.4rem',
      }}
    >
      <TimelineEvent
        title="Carracci Stazione AV"
        createdAt="17:54"
        bubbleStyle={{
          border: '2px solid rgb(36, 101, 137)',
        }}
        icon={<FontAwesomeIcon icon={faFlag} />}
      >
        <StopInstructionContainer>
          <StopInstruction>
            <span className="label">Affluenza </span>
            <span className="value high">Elevata</span>
          </StopInstruction>
          <StopInstruction>
            <span className="label">Ritardo previsto </span>
            <span className="value">10mins</span>
          </StopInstruction>
        </StopInstructionContainer>
      </TimelineEvent>
      <TimelineEvent
        title="Indipendenza"
        createdAt="18:10"
        bubbleStyle={{
          border: `2px solid ${theme.success}`,
          backgroundColor: theme.success,
          color: '#fff',
          fill: '#fff',
        }}
        icon={<FontAwesomeIcon icon={faFlag} />}
      >
        <StopInstructionContainer>
          <StopInstruction>
            <span className="label">Affluenza </span>
            <span className="value high">Elevata</span>
          </StopInstruction>
          <StopInstruction>
            <span className="label">Ritardo previsto </span>
            <span className="value">6mins</span>
          </StopInstruction>
        </StopInstructionContainer>
      </TimelineEvent>
      <TimelineEvent
        title="Piazza Maggiore"
        createdAt="18:25"
        bubbleStyle={{
          border: '2px solid rgb(36, 101, 137)',
        }}
        icon={<FontAwesomeIcon icon={faFlag} />}
      >
        <StopInstructionContainer>
          <StopInstruction>
            <span className="label">Affluenza </span>
            <span className="value medium">Media</span>
          </StopInstruction>
          <StopInstruction>
            <span className="label">Ritardo previsto </span>
            <span className="value">5mins</span>
          </StopInstruction>
        </StopInstructionContainer>
      </TimelineEvent>
      <TimelineEvent
        title="Piazza Cavour - Fine corsa"
        createdAt="18:45"
        bubbleStyle={{
          border: '2px solid rgb(36, 101, 137)',
        }}
        icon={<FontAwesomeIcon icon={faFlagCheckered} />}
      >
        <StopInstructionContainer>
          <StopInstruction>
            <span className="label">Affluenza </span>
            <span className="value high">Elevata</span>
          </StopInstruction>
          <StopInstruction>
            <span className="label">Ritardo previsto </span>
            <span className="value">5mins</span>
          </StopInstruction>
        </StopInstructionContainer>
      </TimelineEvent>
    </Timeline>
  </TimeLineContainer>

);

export default TimeLine;
