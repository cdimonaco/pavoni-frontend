import React from 'react';
import {
  styled, FormGroup, Label, Input, Button,
} from '@smooth-ui/core-em';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBus } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import { theme } from '../../theme/default';

const Wrapper = styled.div`
  height: 100vh;
  background-color: ${theme.primary};
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Alert = styled.div`
 display: block;
 background-color: red;
 padding-left: 50px;
 padding-right: 50px;
 border-radius: 50px;
 margin-bottom: 1rem;
 animation: 0.4s appear ease-in-out;

 @keyframes appear {
   0% {
     opacity: 0.40;
   }

   50%{
     opacity: 0.80;
   }

   100%{
     opacity: 1;
   }
 }

`;

const UserIcon = styled.div`
  font-size: 53px;
  margin-top: 40px;
  color: ${theme.almostWhite};
  border: 2px solid ${theme.almostWhite};
  width: 100px;
  height: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
`;

const Form = styled.form`
  width: 80%;
  max-width: 500px;
  color: ${theme.almostWhite};
`;

const Submit = styled.div`
  display: block;
  button {
    background-color: ${theme.secondary};
    width: 100%;
    min-height: 38px;
    &:hover, &:active {
      background-color: ${theme.secondary};
    }
  }
`;

const Register = styled.div`
  display: block;
  margin-top: 2rem;
  color: ${theme.almostWhite};
`;

const H1 = styled.h1`
  color: #fff;
  transform: translate(100%, 0);
  transition: transform 0.5s ${theme.easticEase}; 
  &.in {
    transform: translate(0, 0);
  }
  svg, span {
    color: ${theme.secondary};
  }
`;

const Login = (props) => {
  const titleEl = React.useRef(null);
  const [username, setUserName] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [loginError, setLoginError] = React.useState(false);

  React.useEffect(
    () => {
      titleEl.current.classList.add('in');
    }, [],
  );

  return (
    <Wrapper>
      <UserIcon>
        <FontAwesomeIcon icon={faBus} />
      </UserIcon>

      <H1 ref={titleEl}>
        <span>S</span>
        mar
        <span>T</span>
        icket
        {' '}
      </H1>
      {loginError && (
        <Alert>
          Errore durante il login
        </Alert>
      )}
      <Form onSubmit={(e) => {
        e.preventDefault();
        if ((username === 'admin@hackmover.it' && password === 'password') || username === 'pavoni') {
          setLoginError(false);
          props.history.push('/dashboard');
          return;
        }
        setLoginError(true);
      }}
      >

        <FormGroup>
          <Label htmlFor="form-group-input-name">Indirizzo email</Label>
          <Input
            control
            id="form-group-input-name"
            value={username}
            onChange={e => setUserName(e.target.value)}
            placeholder="user@email.com"
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="form-group-input-firstname">Password</Label>
          <Input
            value={password}
            control
            onChange={e => setPassword(e.target.value)}
            id="form-group-input-firstname"
            type="password"
            placeholder="your password"
          />
        </FormGroup>
        <Submit>
          <Button>Accedi</Button>
        </Submit>
      </Form>

      <Register>
        <div>Non hai un account?</div>
        <Button>Registrati</Button>
      </Register>
    </Wrapper>
  );
};

Login.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

export default Login;
