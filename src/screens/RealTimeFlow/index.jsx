import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { useGeolocation } from 'react-use';
import LoadingSpinner from '../../components/LoadingSpinner';
import EnhancedMap from '../../components/Maps/EnhancedMap';
import BusStopMarker from '../../components/Maps/BusStopMarker';
import googleMapApiKey from '../../constants/googleMapApiKey';

const TitleContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;
const LocationWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  gap: 2rem;

  @media (min-width: 768px) {
      grid-template-columns: 2fr 1fr;
      gap: 2rem;
  }
`;

const mockedBus = [
  {
    location: {
      lat: 44.517631,
      lon: 11.360834,
    },
    name: 'BUS 6698',
    flowRate: 'Alta affluenza',
    company: 'TPER',
    id: '3311',
  },
  {
    location: {
      lat: 44.517912,
      lon: 11.360358,
    },
    name: 'BUS 6607',
    company: 'TPER',
    flowRate: 'Alta affluenza',
    id: '3312',
  },
  {
    location: {
      lat: 44.517707,
      lon: 11.359653,
    },
    name: 'BUS 6605',
    flowRate: 'Alta affluenza',
    company: 'TPER',
    id: '4313',
  },
];

const RealTimeFlow = ({
  location: {
    state: {
      currentLine,
    },
  },
}) => {
  const { latitude, longitude } = useGeolocation({ enableHighAccuracy: true });

  return (
    <>
      <TitleContainer>
        <h3>
          {currentLine.name}
          {' '}
-
          {' '}
          {currentLine.number}
        </h3>
        <h3>
        Affluenza linea
        </h3>
      </TitleContainer>
      <LocationWrapper>
        {latitude
          ? (
            <div>
              <EnhancedMap
                googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${googleMapApiKey}`}
                loadingElement={<div style={{ height: '100%' }} />}
                containerElement={<div style={{ height: '300px' }} />}
                mapElement={<div style={{ height: '100%' }} />}
                userPosition={{
                  latitude,
                  longitude,
                }}
                positions={mockedBus}
                MarkerComponent={BusStopMarker}
              />
            </div>
          )
          : <LoadingSpinner />}
      </LocationWrapper>
    </>
  );
};

RealTimeFlow.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.shape({
      currentStop: PropTypes.shape({
        name: PropTypes.string,
        number: PropTypes.string,
      }),
    }),
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      lineId: PropTypes.string,
    }),
  }).isRequired,
};

export default RealTimeFlow;
