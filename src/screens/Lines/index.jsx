/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';


const LinesListContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const TitleContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const LinesShow = styled.ul`
  list-style: none;
  padding: 0;
  margin: 2rem 0 0 0;

  li {
    padding: 0.6rem;
    background: white;
    display: flex;
    justify-content: space-between;
    margin-bottom: 1.2rem;
    font-size: larger;
    box-shadow: 0 0.9px 0 0 black;
    
    .line-number{
      color: red;
      margin-left: 1.5rem;
      /* flex: 1; */
      width: 70%;
    }

    .line-name{
      margin-right: 2rem;
      /* flex: 0.4; */
      width: 60%;
    }
  }
`;

const LinesList = ({
  location: {
    state: {
      currentStop,
    },
  },
  match: {
    params: {
      stopId,
    },
  },
  history,
}) => (
  <LinesListContainer>
    <TitleContainer>
      <h3>
        {currentStop.name}
        {' '}
-
        {' '}
        {currentStop.company}
      </h3>
      <h3>
        Linee disponibili
      </h3>
    </TitleContainer>
    <LinesShow>
      <li
        onClick={() => history.push(`/stops/${stopId}/lines/1`, {
          currentLine: {
            name: 'LINEA 1',
            number: '1',
          },
        })}
      >
        <span className="line-number">
          1
        </span>
        <span className="line-name">
          LINEA 1
        </span>
      </li>
      <li>
        <span className="line-number">
          2
        </span>
        <span className="line-name">
          LINEA 2
        </span>
      </li>
      <li>
        <span className="line-number">
          3
        </span>
        <span className="line-name">
          LINEA 3
        </span>
      </li>
      <li>
        <span className="line-number">
          4
        </span>
        <span className="line-name">
          LINEA 4
        </span>
      </li>
      <li>
        <span className="line-number">
          5
        </span>
        <span className="line-name">
          LINEA 5
        </span>
      </li>
    </LinesShow>
  </LinesListContainer>
);

LinesList.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.shape({
      currentStop: PropTypes.shape({
        name: PropTypes.string,
        company: PropTypes.string,
      }),
    }),
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      stopId: PropTypes.string,
    }),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default LinesList;
