export const theme = {
  primaryLighter: '#a0ccff',
  primary: '#4B95EA',
  success: '#157d26',
  dark: '#11111C',
  gray: '#939393',
  almostWhite: '#F2F2F4',
  secondary: '#FBB714',
  headerHeight: '70px',
  easticEase: 'cubic-bezier(0.64, 0.57, 0.67, 1.53)',
};


export default theme;
